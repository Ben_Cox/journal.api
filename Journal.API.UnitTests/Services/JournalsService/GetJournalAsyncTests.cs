﻿using AutoFixture;
using AutoMapper;
using Journal.API.Infrastructure.Entities.Journal;
using Journal.API.Infrastructure.Repositories.Contracts;
using Journal.API.Infrastructure.Services.Contracts;
using Moq;
using NUnit.Framework;

namespace Journal.API.UnitTests.Services.JournalsService
{
    [TestFixture]
    public class GetJournalAsyncTests
    {
        private readonly IFixture _fixture = new Fixture();
        private IMapper _mapper = null!;

        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            // Set up AutoMapper to run as normal for the test
            var mapperConfig = new MapperConfiguration(config =>
            {
                config.AddMaps(new[]
                {
                    "Journal.API.Infrastructure"
                });
            });
            _mapper = mapperConfig.CreateMapper();
        }


        [Test]
        public void GetJournalAsync_WhenUserIdOutOfRange_ShouldArgumentOutOfRangeException()
        {
            // Arrage

            IJournalsService journalsService = new Infrastructure.Services.JournalsService(null!, null!);

            // Act & Assert

            Assert.ThrowsAsync<ArgumentOutOfRangeException>(async () => await journalsService.GetJournalAsync(-1));
        }

        [Test]
        public async Task GetJournalAsync_WhenIdOk_ShouldReturnDTO()
        {
            // Arrange

            int idMock = _fixture.Create<int>();

            JournalEntity entityMock = _fixture.Build<JournalEntity>()
                .With(j => j.Id, idMock)
                .Create();

            Mock<IJournalRepository> journalsRepositoryMock = new Mock<IJournalRepository>();
            journalsRepositoryMock
                .Setup(jrm => jrm.SelectJournalAsync(idMock))
                .ReturnsAsync(entityMock)
                .Verifiable();

            IJournalsService journalsService = new Infrastructure.Services.JournalsService(journalsRepositoryMock.Object, _mapper);


            // Act

            var result = await journalsService.GetJournalAsync(idMock);

            // Assert

            Assert.NotNull(result);
            Assert.AreEqual(entityMock.Id, result.Id);
            Assert.AreEqual(entityMock.Title, result.Title);
            Assert.AreEqual(entityMock.AuthorUserName, result.AuthorUserName);
            Assert.AreEqual(entityMock.Date, result.Date);
            Assert.AreEqual(entityMock.Content, result.Content);
        }
    }
}
