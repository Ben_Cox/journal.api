﻿namespace Journal.API.Infrastructure.Entities.User
{
    public class UserEntity
    {
        public int Id { get; set; }

        public string UserName { get; set; } = null!;

        public bool IsAdmin { get; set; }
    }
}
