﻿namespace Journal.API.Infrastructure.Entities.Journal
{
    public class JournalSummaryEntity
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; } = null!;

        public string AuthorUserName { get; set; } = null!;
    }
}
