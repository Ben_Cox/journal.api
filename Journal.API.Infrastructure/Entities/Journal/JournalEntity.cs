﻿namespace Journal.API.Infrastructure.Entities.Journal
{
    public class JournalEntity
    {
        public int Id { get; set; }
        public string Title { get; set; } = null!;
        public DateTime Date { get; set; }
        public DateTime CreatedAt { get; set; }
        public string Content { get; set; } = null!;
        public int AuthorUserId { get; set; }
        public string AuthorUserName { get; set; } = null!;
    }
}
