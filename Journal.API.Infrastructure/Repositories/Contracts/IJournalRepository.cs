﻿using Journal.API.Infrastructure.Entities.Journal;

namespace Journal.API.Infrastructure.Repositories.Contracts
{
    public interface IJournalRepository
    {
        public Task<JournalEntity> SelectJournalAsync(int id);
        public Task<List<JournalSummaryEntity>> SelectJournalSummariesAsync(int userId, int page, int pageSize);
        public Task<int> InsertJournalAsync(JournalEntity entity);
    }
}
