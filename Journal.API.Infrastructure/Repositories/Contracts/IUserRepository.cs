﻿using Journal.API.Infrastructure.Entities.User;

namespace Journal.API.Infrastructure.Repositories.Contracts
{
    public interface IUserRepository
    {
        public Task<UserEntity?> SelectUserAsync(int? id, string? userName, string? password);
    }
}
