﻿using AutoMapper;
using Journal.API.Infrastructure.Entities.Journal;
using Journal.API.Infrastructure.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;

namespace Journal.API.Infrastructure.Repositories
{
    public class JournalRepository : IJournalRepository
    {
        private readonly string _connectionString;

        private readonly IMapper _mapper;

        public JournalRepository(IConfiguration config, IMapper mapper)
        {
            _connectionString = config["ConnectionString"];
            _mapper = mapper;
        }

        public async Task<JournalEntity> SelectJournalAsync(int id)
        {
            SqlParameter[] parameters = new SqlParameter[] {
               new SqlParameter("@ID", id),
            };

            var dt = await RepositoryHelper.ExecuteProcedureAsync(_connectionString, "SelectJournalByID", parameters);
            if (dt == null || dt.Rows.Count != 1)
                throw new Exception("Something went wrong SelectJournalByID");

            return _mapper.Map<JournalEntity>(dt.Rows[0]);
        }

        public async Task<List<JournalSummaryEntity>> SelectJournalSummariesAsync(int userId, int page, int pageSize)
        {
            SqlParameter[] parameters = new SqlParameter[] {
               new SqlParameter("@UserID", userId),
               new SqlParameter("@PageNumber", page),
               new SqlParameter("@PageSize", pageSize),
            };

            var dt = await RepositoryHelper.ExecuteProcedureAsync(_connectionString, "SelectJournalSummaries", parameters);

            if (dt == null || dt.Rows.Count == 0)
                return new List<JournalSummaryEntity>();

            List<JournalSummaryEntity> results = new List<JournalSummaryEntity>();
            foreach (var dr in dt.Rows)
                results.Add(_mapper.Map<JournalSummaryEntity>(dr));

            return results;
        }

        public async Task<int> InsertJournalAsync(JournalEntity entity)
        {
            SqlParameter[] parameters = new SqlParameter[] {
               new SqlParameter("@UserID", entity.AuthorUserId),
               new SqlParameter("@Title", entity.Title),
               new SqlParameter("@Content", entity.Content),
               new SqlParameter("@Date", entity.Date),
            };

            var dt = await RepositoryHelper.ExecuteProcedureAsync(_connectionString, "InsertJournal", parameters);
            if (dt == null || dt.Rows.Count != 1)
                throw new Exception("Something went wrong InsertJournal");

            return dt.Rows[0].Field<int>("ID");
        }
    }
}
