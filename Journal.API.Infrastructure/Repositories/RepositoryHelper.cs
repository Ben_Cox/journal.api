﻿using System.Data;
using System.Data.SqlClient;

namespace Journal.API.Infrastructure.Repositories
{
    internal static class RepositoryHelper
    {
        internal static async Task<DataTable> ExecuteProcedureAsync(string connectionString, string procedureName, SqlParameter[]? parameters)
        {
            DataTable dt = new DataTable();
            using var con = new SqlConnection(connectionString);
            try
            {
                using var cmd = new SqlCommand(procedureName, con);
                cmd.CommandType = CommandType.StoredProcedure;

                if (parameters != null)
                    cmd.Parameters.AddRange(parameters);

                await con.OpenAsync();
                dt.Load(await cmd.ExecuteReaderAsync());
                await con.CloseAsync();
            }
            finally
            {
                await con.CloseAsync();
            }
            return dt;
        }
    }
}
