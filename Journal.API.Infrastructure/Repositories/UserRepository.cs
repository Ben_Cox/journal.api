﻿using AutoMapper;
using Journal.API.Infrastructure.Entities.User;
using Journal.API.Infrastructure.Repositories.Contracts;
using Microsoft.Extensions.Configuration;
using System.Data.SqlClient;

namespace Journal.API.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly string _connectionString;
        private readonly IMapper _mapper;

        public UserRepository(IConfiguration config, IMapper mapper)
        {
            _connectionString = config["ConnectionString"];
            _mapper = mapper;
        }

        public async Task<UserEntity?> SelectUserAsync(int? id, string? userName, string? password)
        {
            List<SqlParameter> parameters = new List<SqlParameter>();

            if (userName != null)
                parameters.Add(new SqlParameter("@UserName", userName));

            if (password != null)
                parameters.Add(new SqlParameter("@Password", password));

            if (id != null)
                parameters.Add(new SqlParameter("@ID", id));

            var dt = await RepositoryHelper.ExecuteProcedureAsync(_connectionString, "SelectUser", parameters.ToArray());
            if (dt == null || dt.Rows.Count < 1)
                return null;

            return _mapper.Map<UserEntity>(dt.Rows[0]);
        } 
    }
}
