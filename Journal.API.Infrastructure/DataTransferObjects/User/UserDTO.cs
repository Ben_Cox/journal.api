﻿namespace Journal.API.Infrastructure.DataTransferObjects.User
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string UserName { get; set; } = null!;
        public bool IsAdmin { get; set; }
    }
}
