﻿namespace Journal.API.Infrastructure.DataTransferObjects.Journal
{
    public class JournalSummaryDTO
    {
        public int Id { get; set; }

        public DateTime Date { get; set; }

        public string Title { get; set; } = null!;

        public string AuthorUserName { get; set; } = null!;
    }
}
