﻿namespace Journal.API.Infrastructure.DataTransferObjects.Journal
{
    public class JournalDTO
    {
        public int? Id { get; set; }

        public string? AuthorUserName { get; set; }
        public string Title { get; set; } = null!;
        public DateTime? CreatedAt { get; set; }
        public DateTime Date { get; set; }
        public string Content { get; set; } = null!;
    }
}
