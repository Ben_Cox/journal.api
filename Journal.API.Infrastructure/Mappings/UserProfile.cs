﻿using AutoMapper;
using Journal.API.Infrastructure.DataTransferObjects.User;
using Journal.API.Infrastructure.Entities.User;
using System.Data;

namespace Journal.API.Infrastructure.Mappings
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserEntity, UserDTO>().ReverseMap();

            CreateMap<DataRow, UserEntity>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Field<int>("ID")))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Field<string>("UserName")))
                .ForMember(dest => dest.IsAdmin, opt => opt.MapFrom(src => src.Field<bool>("IsAdmin")));
        }
    }
}
