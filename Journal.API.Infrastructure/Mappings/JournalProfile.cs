﻿using AutoMapper;
using Journal.API.Infrastructure.DataTransferObjects.Journal;
using Journal.API.Infrastructure.Entities.Journal;
using System.Data;

namespace Journal.API.Infrastructure.Mappings
{
    public class JournalProfile : Profile
    {
        public JournalProfile()
        {
            CreateMap<JournalDTO, JournalEntity>().ReverseMap();

            CreateMap<DataRow, JournalEntity>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Field<int>("ID")))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Field<string>("Title")))
                .ForMember(dest => dest.AuthorUserId, opt => opt.MapFrom(src => src.Field<int>("AuthorUserID")))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Field<DateTime>("Date")))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Field<DateTime>("CreatedAt")))
                .ForMember(dest => dest.AuthorUserName, opt => opt.MapFrom(src => src.Field<string>("AuthorUserName")))
                .ForMember(dest => dest.Content, opt => opt.MapFrom(src => src.Field<string>("Content")));
        }
    }
}
