﻿using AutoMapper;
using Journal.API.Infrastructure.DataTransferObjects.Journal;
using Journal.API.Infrastructure.Entities.Journal;
using System.Data;

namespace Journal.API.Infrastructure.Mappings
{
    public class JournalSummaryProfile : Profile
    {
        public JournalSummaryProfile()
        {
            CreateMap<JournalSummaryEntity, JournalSummaryDTO>().ReverseMap();

            CreateMap<DataRow, JournalSummaryEntity>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.Field<int>("ID")))
                .ForMember(dest => dest.Title, opt => opt.MapFrom(src => src.Field<string>("Title")))
                .ForMember(dest => dest.AuthorUserName, opt => opt.MapFrom(src => src.Field<string>("AuthorUserName")))
                .ForMember(dest => dest.Date, opt => opt.MapFrom(src => src.Field<DateTime>("Date")));
        }
    }
}
