
-- Create Tables
Create Table dbo.[User]
(
	ID Int Identity Not Null,
	UserName Varchar(256) Not Null,
	[Password] NVarchar(Max) Not Null,
	IsAdmin Bit Not Null, -- Ideally would have dbo.Role table, with Role_User_Link table for who's what...
	Constraint PK_User Primary Key (ID),
)

Go

Create Table dbo.Journal 
(
	ID Int Identity Not Null,
	Title NVarchar(100) Not Null,
	Content NVarchar(Max) Not Null,
	AuthorUserID Int Not Null,
	[Date] DateTime Not Null,
	CreatedAt DateTime Not Null,
	Constraint PK_Journal Primary Key (ID),
	Constraint FK_Journal_AuthorUserID_User_ID Foreign Key (AuthorUserID) References [User] (ID)
)

Go

-- Set Up Data
-- Obviously not secure!
Insert Into dbo.[User] (UserName, [Password], IsAdmin) Values ('Admin', 'Admin', 1)
Insert Into dbo.[User] (UserName, [Password], IsAdmin) Values ('Normal1', 'Normal1', 0)
Insert Into dbo.[User] (UserName, [Password], IsAdmin) Values ('Normal2', 'Normal2', 0)

Select * From dbo.[User]

-- Clear Down
--Drop Table dbo.Journal
--Drop Table dbo.[User]
--Go