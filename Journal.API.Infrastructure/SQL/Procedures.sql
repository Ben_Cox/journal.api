Create Procedure dbo.SelectUser
(
	@ID Int = Null,
	@UserName Varchar(256) = Null,
	@Password NVarchar(Max) = Null
)
As
Begin
	Select
		ID,
		UserName,
		IsAdmin
	From dbo.[User]
	Where
		(@UserName Is Not Null And @Password Is Not Null And
		UserName = @UserName And [Password] = @Password)
		Or
		(@ID Is Not Null And Id = @ID)
End

Go

Create Procedure dbo.SelectJournalByID
(
	@ID Int
)
As
Begin
	Select
		J.ID,
		J.Title,
		J.[Date],
		J.CreatedAt,
		J.Content,
		J.AuthorUserID,
		U.UserName As AuthorUserName
	From dbo.[Journal] J
	Inner Join dbo.[User] U
		On J.AuthorUserID = u.ID
	Where
		J.ID = @ID
End

Go

--Exec dbo.SelectJournalSummaries @UserID = 1, @PageNumber = 1, @PageSize = 100 
Create Procedure dbo.SelectJournalSummaries
(
	@UserID Int,
	@PageNumber Int,
	@PageSize Int
)
As
Begin
	Select
		J.ID,
		J.Title,
		J.[Date],
		JU.UserName As AuthorUserName
	From dbo.[Journal] J
	Inner Join dbo.[User] JU
		On J.AuthorUserID = JU.ID
	Inner Join dbo.[User] U
		On U.ID = @UserID
	Where
		J.AuthorUserID = @UserID Or U.IsAdmin = 1 -- Obviously would be roles table
	Order By
		J.[Date] Desc
	-- Pagination
	Offset @PageSize * (@PageNumber - 1) Rows
	Fetch Next @PageSize Rows Only	
End

Go

Create Procedure dbo.InsertJournal
(
	@UserID Int,
	@Title NVarchar(100),
	@Content NVarchar(Max),
	@Date DateTime
)
As
Begin

	Declare @OutputTable Table (ID Int)

	Insert Into dbo.Journal (Title, Content, AuthorUserID, [Date], CreatedAt)
		Output Inserted.ID Into @OutputTable
	Values (@Title, @Content, @UserID, @Date, GETDATE())

	-- @@Identity has issues -> https://stackoverflow.com/a/42655
	Select Top 1 ID From @OutputTable
End

-- Clear Down
--Drop Procedure dbo.SelectUser
--Drop Procedure dbo.SelectJournalByID
--Drop Procedure dbo.SelectJournalSummaries
--Drop Procedure dbo.InsertJournal