﻿using AutoMapper;
using Journal.API.Infrastructure.DataTransferObjects.Journal;
using Journal.API.Infrastructure.Entities.Journal;
using Journal.API.Infrastructure.Repositories.Contracts;
using Journal.API.Infrastructure.Services.Contracts;
using Microsoft.Extensions.Caching.Memory;

namespace Journal.API.Infrastructure.Services
{
    public class JournalsService : IJournalsService
    {
        private readonly IJournalRepository _journalRepository;
        private readonly IMapper _mapper;

        public JournalsService(IJournalRepository journalRepository, IMapper mapper)
        {
            _journalRepository = journalRepository;
            _mapper = mapper;
        }

        public async Task<JournalDTO> CreateJournalAsync(int userId, JournalDTO journal)
        {
            if (userId <= 0)
                throw new ArgumentOutOfRangeException(nameof(userId));

            if (journal == null)
                throw new ArgumentNullException(nameof(journal));

            if (string.IsNullOrEmpty(journal.Content))
                throw new ArgumentException(nameof(journal.Content));

            if (string.IsNullOrWhiteSpace(journal.Title))
                throw new ArgumentException(nameof(journal.Title));

            if (journal.Title.Length > 100)
                throw new ArgumentOutOfRangeException(nameof(journal.Title.Length));

            var journalEntity = _mapper.Map<JournalEntity>(journal);
            journalEntity.AuthorUserId = userId;

            int newId = await _journalRepository.InsertJournalAsync(journalEntity);

            JournalEntity newEntity = await _journalRepository.SelectJournalAsync(newId);
            return _mapper.Map<JournalDTO>(newEntity);
        }

        public async Task<JournalDTO> GetJournalAsync(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException(nameof(id));

            var entity = await _journalRepository.SelectJournalAsync(id);
            return _mapper.Map<JournalDTO>(entity);
        }

        public async Task<List<JournalSummaryDTO>> GetJournalSummariesAsync(int userId, int pageNumber, int pageSize)
        {
            if (userId <= 0)
                throw new ArgumentOutOfRangeException(nameof(userId));

            if (pageNumber <= 0)
                throw new ArgumentOutOfRangeException(nameof(pageNumber));

            if (pageSize <= 0)
                throw new ArgumentOutOfRangeException(nameof(pageSize));
            
            var entities = await _journalRepository.SelectJournalSummariesAsync(userId, pageNumber, pageSize);

            return _mapper.Map<List<JournalSummaryDTO>>(entities);
        }
    }
}
