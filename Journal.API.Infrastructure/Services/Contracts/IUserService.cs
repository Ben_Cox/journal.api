﻿using Journal.API.Infrastructure.DataTransferObjects.User;

namespace Journal.API.Infrastructure.Services.Contracts
{
    public interface IUserService
    {
        public Task<UserDTO?> SelectUserAsync(int id);
        public Task<UserDTO?> SelectUserAsync(string userName, string password);
    }
}
