﻿using Journal.API.Infrastructure.DataTransferObjects.Journal;

namespace Journal.API.Infrastructure.Services.Contracts
{
    public interface IJournalsService
    {
        public Task<JournalDTO> GetJournalAsync(int id);
        public Task<List<JournalSummaryDTO>> GetJournalSummariesAsync(int userId, int pageNumber, int pageSize);
        public Task<JournalDTO> CreateJournalAsync(int userId, JournalDTO journal);
    }
}
