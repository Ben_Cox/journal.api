﻿using AutoMapper;
using Journal.API.Infrastructure.DataTransferObjects.User;
using Journal.API.Infrastructure.Repositories.Contracts;
using Journal.API.Infrastructure.Services.Contracts;

namespace Journal.API.Infrastructure.Services
{
    public class UserService : IUserService
    {

        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository, IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDTO?> SelectUserAsync(int id)
        {
            if (id <= 0)
                throw new ArgumentOutOfRangeException(nameof(id));
            var entity = await _userRepository.SelectUserAsync(id, null, null);

            return _mapper.Map<UserDTO>(entity);
        }

        public async Task<UserDTO?> SelectUserAsync(string userName, string password)
        {
            if (string.IsNullOrWhiteSpace(userName))
                throw new ArgumentNullException(userName);

            if (string.IsNullOrWhiteSpace(password))
                throw new ArgumentNullException(password);

            // password => would be encrypted/hashed at this point...

            var entity = await _userRepository.SelectUserAsync(null, userName, password);

            return _mapper.Map<UserDTO>(entity);
        }
    }
}
