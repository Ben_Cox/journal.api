﻿namespace Journal.API.Models
{
    public class AuthenticateParameters
    {
        public string Password { get; set; } = null!;
        public string UserName { get; set; } = null!;
    }
}
