﻿using Journal.API.Infrastructure.DataTransferObjects.Journal;
using Journal.API.Infrastructure.Services.Contracts;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;

namespace Journal.API.Controllers
{
    [Authorize]
    [Route("api/[controller]")]
    [ApiController]
    public class JournalsController : ControllerBase
    {
        private readonly IJournalsService _JournalService;
        private readonly IMemoryCache _cache;

        public JournalsController(IJournalsService journalsService, IMemoryCache cache)
        {
            _JournalService = journalsService;
            _cache = cache;
        }

        [HttpPost]
        public async Task<ActionResult<JournalDTO>> PostJournal([FromBody]JournalDTO journal)
        {
            int userId = int.Parse(User.Claims.First(c => c.Type == "uid").Value);
            JournalDTO newJournal = await _JournalService.CreateJournalAsync(userId, journal);
            return new OkObjectResult(newJournal);
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<JournalDTO>> GetJournal(int id)
        {
            // Obviously would be in it's own CachedService IoC & Update would reset this.
            var dto = await _cache.GetOrCreateAsync("Journal:" + id, async ce =>
            {
                ce.AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1);
                return await _JournalService.GetJournalAsync(id);
            });

            return new OkObjectResult(dto);
        }

        [HttpGet("summaries")]
        public async Task<ActionResult<List<JournalSummaryDTO>>> GetSummaries([FromQuery]int pageNumber, [FromQuery]int pageSize)
        {
            int userId = int.Parse(User.Claims.First(c => c.Type == "uid").Value);
            var summaries = await _JournalService.GetJournalSummariesAsync(userId, pageNumber, pageSize);
            return new OkObjectResult(summaries);
        }
    }
}
