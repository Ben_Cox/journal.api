﻿using Journal.API.Infrastructure.DataTransferObjects.User;
using Journal.API.Infrastructure.Services.Contracts;
using Journal.API.Models;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System.Security.Claims;

namespace Journal.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IMemoryCache _cache;

        public UsersController(IUserService userService, IMemoryCache cache)
        {
            _userService = userService;
            _cache = cache;
        }

        [HttpGet]
        public async Task<ActionResult<UserDTO>> Get()
        {
            if (User.Identity?.IsAuthenticated == true)
            {

                int userId = int.Parse(User.Claims.First(c => c.Type == "uid").Value);

                var user = await _cache.GetOrCreateAsync("User:" + userId, async ce =>
                {
                    ce.AbsoluteExpirationRelativeToNow = TimeSpan.FromDays(1);
                    return await _userService.SelectUserAsync(userId);
                });

                return new OkObjectResult(user);
            }
            else
            {
                return new NoContentResult();
            }
        }

        [HttpPost("authenticate")]
        public async Task<ActionResult<UserDTO>> PostAuthenticate([FromBody]AuthenticateParameters parameters)
        {
            var existingUser = await _userService.SelectUserAsync(parameters.UserName, parameters.Password);

            if (existingUser == null)
                return new NotFoundResult();

            var claims = new ClaimsIdentity(new[]
            {
                new Claim("uid", existingUser.Id.ToString()),
                new Claim(ClaimTypes.Name, existingUser.UserName),
                new Claim(ClaimTypes.Role, existingUser.IsAdmin ? "Admin" : "Normal"),
            }, CookieAuthenticationDefaults.AuthenticationScheme);

            await HttpContext.SignInAsync(CookieAuthenticationDefaults.AuthenticationScheme, new ClaimsPrincipal(claims), new AuthenticationProperties
            {
                IsPersistent = true,
                AllowRefresh = true,
                ExpiresUtc = DateTimeOffset.UtcNow.AddYears(1)
            });

            return new OkObjectResult(existingUser);
        }

        [HttpDelete("authenticate")]
        public async Task<ActionResult> DeleteAuthenticate()
        {
            await HttpContext.SignOutAsync(CookieAuthenticationDefaults.AuthenticationScheme);
            return NoContent();
        }
    }
}
