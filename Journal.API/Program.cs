using Journal.API.Infrastructure.Repositories;
using Journal.API.Infrastructure.Repositories.Contracts;
using Journal.API.Infrastructure.Services;
using Journal.API.Infrastructure.Services.Contracts;
using Microsoft.AspNetCore.Authentication.Cookies;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddMemoryCache();
builder.Services.AddCors();

// AutoMapper
builder.Services.AddAutoMapper(config =>
{
    config.AddMaps(new[]
    {
        "Journal.API.Infrastructure"
    });
});

builder.Services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
    .AddCookie(options =>
    {
        options.LoginPath = "/signin/";
        options.Cookie.SameSite = SameSiteMode.None;
    });

builder.Services
    .AddTransient<IUserService, UserService>()
    .AddTransient<IUserRepository, UserRepository>()
    .AddTransient<IJournalsService, JournalsService>()
    .AddTransient<IJournalRepository, JournalRepository>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

// Obviously would be in config
app.UseCors(policy =>
{
    policy.AllowAnyHeader();
    policy.AllowAnyMethod();
    policy.WithOrigins(new[]
    {
        "http://localhost:3000",
        "https://localhost:3000",
        "http://bencox-journal.azurewebsites.net",
        "https://bencox-journal.azurewebsites.net"
    });
    policy.AllowCredentials();
});

app.UseHttpsRedirection();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllers();

app.Run();
